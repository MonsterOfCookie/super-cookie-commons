package com.supercookie.logging;

import com.badlogic.gdx.Gdx;


class SimpleLogger implements Logger {

    private final String className;
    private final String logHandle;

    public SimpleLogger(String className,  String logHandle) {
        this.className = className;
        this.logHandle = logHandle;
    }

    @Override
    public void debug(String text, Object... args) {
        for (Object object : args) {
            text = text.replaceFirst("\\{\\}", object.toString());
        }
        Gdx.app.debug(logHandle + "_" + className, text);
    }
}
