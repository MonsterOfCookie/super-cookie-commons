package com.supercookie.logging;

/**
 * Created by Cookie on 08/01/2015.
 */
public interface Logger {
    void debug(String text, Object... args);
}
