package com.supercookie.logging;

/**
 * Created by Cookie on 08/01/2015.
 */
public final class LoggerFactory {

    public static String LOG_HANDLE = "LOG";

    public static Logger getLogger(Class clazz) {
        return new SimpleLogger(clazz.getSimpleName(), LOG_HANDLE);
    }
}
