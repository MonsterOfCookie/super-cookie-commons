package com.supercookie.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

/**
 * Created by Cookie on 08/01/2015.
 */
public class LabelFactory {

    private final AssetManager assetManager;

    public LabelFactory(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public Label generateLabel(String fontName, String text) {
        return new Label(text, getLabelStyle(fontName));
    }

    public Label generateLabel(String font, String text, Color color) {
        return new Label(text, getLabelStyle(font, color));
    }

    private Label.LabelStyle getLabelStyle(String font) {
        return getLabelStyle(font, Color.WHITE);
    }

    public Label.LabelStyle getLabelStyle(String font, Color color) {
        return new Label.LabelStyle(assetManager.get(font, BitmapFont.class), color);
    }

}
