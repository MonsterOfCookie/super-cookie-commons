package com.supercookie.assets;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AssetLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Array;
import com.supercookie.logging.Logger;
import com.supercookie.logging.LoggerFactory;

import java.util.*;

public final class UI {

    private static final Logger LOG = LoggerFactory.getLogger(UI.class);

    private final AssetManager assetManager = new AssetManager();
    private final Set<String> textureAtlasNames = new HashSet<>();
    private final Map<String, String> regionToTextureAtlasName = new HashMap<>();

    public static final UI LOADER = new UI();

    private UI() {
        setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
    }

    public <T, P extends AssetLoaderParameters<T>> void setLoader(Class<T> type, AssetLoader<T, P> loader) {
        this.assetManager.setLoader(type, loader);
    }

    public <T> T getAsset(String name, Class<T> type) {
        return assetManager.get(name, type);
    }

    public <T> Array<T> getAll(Class<T> type, Array<T> out) {
        return assetManager.getAll(type, out);
    }

    public ButtonFactory buttonFactory() {
        return new ButtonFactory(LOADER);
    }

    public ImageFactory imageFactory() {
        return new ImageFactory(LOADER);
    }

    public LabelFactory labelFactory() {
        return new LabelFactory(assetManager);
    }

    public Audio audio() {
        return new Audio(assetManager);
    }

    TextureAtlas.AtlasRegion findRegion(String name) {
        return textureAtlas(findTextureAtlasName(name)).findRegion(name);
    }

    Texture doLoad(String name) {
        if (assetManager.isLoaded(name)) {
            return assetManager.get(name, Texture.class);
        } else {
            LOG.debug("Asset {} not loaded", name);
            throw new IllegalStateException("Asset " + name + " is not loaded!");
        }
    }

    public TiledMap tiledMap(String name) {
        return assetManager.get(name, TiledMap.class);
    }

    public void assetsToLoad(Map<String, Class> objectsToLoad, Map<String, AssetLoaderParameters> objectsParameters) {
        for (Map.Entry<String, Class> entry : objectsToLoad.entrySet()) {
            LOG.debug("Loading {}-{}", entry.getKey(), entry.getValue());

            if (entry.getValue().equals(Texture.class)) {
                loadTexture(entry.getKey());
            } else {

                if (objectsParameters.containsKey(entry.getKey())) {
                    AssetLoaderParameters assetLoaderParameters = objectsParameters.get(entry.getKey());
                    assetManager.load(entry.getKey(), entry.getValue(), assetLoaderParameters);
                } else {
                    assetManager.load(entry.getKey(), entry.getValue());
                }

                if (entry.getValue().equals(TextureAtlas.class)) {
                    textureAtlasNames.add(entry.getKey());
                }
            }
        }
    }

    public boolean needsToLoad(String name) {
        return !assetManager.isLoaded(name);
    }

    public boolean needsToLoad(Map<String, Class> assets) {
        for (Map.Entry<String, Class> entry : assets.entrySet()) {
            if (!assetManager.isLoaded(entry.getKey(), entry.getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean update() {
        return assetManager.update();
    }

    public float getProgress() {
        return assetManager.getProgress();
    }

    public void dispose(String name) {
        if (assetManager.isLoaded(name)) {
            assetManager.unload(name);
            if (textureAtlasNames.contains(name)) {
                textureAtlasNames.remove(name);
                for (Iterator<String> iter = regionToTextureAtlasName.keySet().iterator(); iter.hasNext(); ) {
                    String key = iter.next();
                    String value = regionToTextureAtlasName.get(key);
                    if (value.equals(name)) {
                        iter.remove();
                    }
                }
            }
        }
    }

    public void disposeAll() {
        for (String name : assetManager.getAssetNames()) {
            dispose(name);
        }
    }

    private void loadTexture(String name) {
        TextureLoader.TextureParameter param = new TextureLoader.TextureParameter();
        param.minFilter = Texture.TextureFilter.Linear;
        param.magFilter = Texture.TextureFilter.Linear;
        assetManager.load(name, Texture.class, param);
    }

    private String findTextureAtlasName(String name) {
        if (regionToTextureAtlasName.containsKey(name)) {
            return regionToTextureAtlasName.get(name);
        } else {
            for (String textureAtlasName : textureAtlasNames) {
                boolean found = textureAtlas(textureAtlasName).findRegion(name) != null;
                if (found) {
                    regionToTextureAtlasName.put(name, textureAtlasName);
                    return textureAtlasName;
                }
            }
        }
        throw new IllegalArgumentException("Unknown region " + name);
    }

    public TextureAtlas textureAtlas(String name) {
        return assetManager.get(name, TextureAtlas.class);
    }

}
