package com.supercookie.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by Cookie on 08/01/2015.
 */
public class ButtonFactory {

    private final UI ui;

    ButtonFactory(UI ui) {
        this.ui = ui;
    }

    public ImageButton createButtonFromAtlas(
            String btnOff, String btnOn, String name) {
        return createButtonFromAtlas(
                btnOff,
                btnOn,
                null,
                name
        );
    }

    public ImageButton createButtonFromAtlas(
            String btnOff, String btnOn, String checked, String name) {
        return createButton(
                new TextureAtlas.AtlasSprite(ui.findRegion(btnOff)),
                new TextureAtlas.AtlasSprite(ui.findRegion(btnOn)),
                checked != null ? new TextureAtlas.AtlasSprite(ui.findRegion(checked)) : null,
                name
        );
    }

    public ImageButton createButton(TextureAtlas.AtlasSprite off, TextureAtlas.AtlasSprite on, TextureAtlas.AtlasSprite checked, final String name) {

        ImageButton button =
                checked != null ?
                        new ImageButton(
                                new SpriteDrawable(off),
                                new SpriteDrawable(on),
                                new SpriteDrawable(checked)) :
                        new ImageButton(
                                new SpriteDrawable(off),
                                new SpriteDrawable(on));

        button.setName(name);
        button.setTransform(true);
        button.setOrigin(button.getWidth() / 2, button.getHeight() / 2);

        return button;
    }

    public ImageButton createButton(Drawable drawable, final String name) {

        ImageButton button = new ImageButton(drawable, drawable);

        button.setName(name);
        button.setTransform(true);
        button.setOrigin(button.getWidth() / 2, button.getHeight() / 2);
        button.getImage().setScaling(Scaling.stretch);
        button.getImage().setFillParent(true);
        button.getImageCell().expand();
        button.getImageCell().fill();

        return button;
    }
}
