package com.supercookie.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;

public final class ImageFactory {

    private final UI ui;

    ImageFactory(UI ui) {
        this.ui = ui;
    }

    public Image loadImageByName(String name, Scaling scaling) {
        Image rtn = new Image(new TextureRegionDrawable(new TextureRegion(ui.doLoad(name))), scaling);
        rtn.setOrigin(rtn.getWidth() / 2, rtn.getHeight() / 2);
        return rtn;
    }

    public Image loadImageFromAtlas(String name) {
        Image rtn = new Image(ui.findRegion(name));
        rtn.setOrigin(rtn.getWidth() / 2, rtn.getHeight() / 2);
        return rtn;
    }

    public TextureAtlas.AtlasRegion loadTextureFromAtlas(String name) {
        return ui.findRegion(name);
    }

    public Texture loadTextureByName(String name) {
        return ui.doLoad(name);
    }


}
