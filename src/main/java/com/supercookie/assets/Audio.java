package com.supercookie.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Audio {

    private final AssetManager assetManager;
    private final Map<String, Sound> sfxCache = new HashMap<>();

    private String currentMusic = "";
    private Music music = null;

    Audio(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public void playMusic(String name) {
        if (!currentMusic.equalsIgnoreCase(name)) {
            if (music != null) {
                music.stop();
            }
            music = assetManager.get(name, Music.class);
            music.setLooping(true);
            music.setVolume(0.15F);
            music.play();
            currentMusic = name;
        } else {
            music.play();
        }
    }

    public void playSound(String name) {
        doLoadSound(name).play(0.30F);
    }

    public void stopMusic() {
        if (music != null) music.stop();
    }

    public void resumeMusic() {
        if (music != null) {
            music.play();
        }
    }

    private Sound doLoadSound(String name) {
        return assetManager.isLoaded(name) ?
                assetManager.get(name, Sound.class) :
                (sfxCache.containsKey(name) ? sfxCache.get(name) : doFreshLoadSound(name));
    }

    private Sound doFreshLoadSound(String name) {
        Sound sound = Gdx.audio.newSound(Gdx.files.internal(name));
        sfxCache.put(name, sound);
        return sound;
    }

    public void soundsToLoad(List<String> soundsToLoad) {
        for (String sound : soundsToLoad) {
            assetManager.load(sound, Sound.class);
        }

    }

    public void musicToLoad(List<String> musicToLoad) {
        for (String sound : musicToLoad) {
            assetManager.load(sound, Music.class);
        }
    }

}
