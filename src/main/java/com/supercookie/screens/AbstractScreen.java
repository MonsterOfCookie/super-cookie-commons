package com.supercookie.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.supercookie.CookieGame;
import com.supercookie.logging.Logger;
import com.supercookie.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class AbstractScreen<T extends CookieGame> extends ScreenAdapter {

    public static int GAME_VIEWPORT_WIDTH = 1080, GAME_VIEWPORT_HEIGHT = 1920;

    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    protected final Stage stage;
    protected final T game;

    public AbstractScreen(T game) {
        this.game = game;
        this.stage = new Stage(new ScalingViewport(Scaling.stretch, GAME_VIEWPORT_WIDTH, GAME_VIEWPORT_HEIGHT));
    }

    public T getGame() {
        return game;
    }

    public Batch getBatch() {
        return stage.getBatch();
    }

    public Stage getStage() {
        return stage;
    }

    protected void update(float delta) {
        stage.act(delta);
    }

    protected void render() {
        stage.draw();
    }

    @Override
    public final void render(
            float delta) {

        GL20 gl = Gdx.graphics.getGL20();
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update(delta);

        if (game.getScreen() == this) render();
    }

    @Override
    public void show() {
        LOG.debug("show");
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(
            int width,
            int height) {
        LOG.debug("resize {}x{}", width, height);
    }


    @Override
    public void hide() {
        LOG.debug("hide");
    }

    @Override
    public void pause() {
        LOG.debug("pause");
    }

    @Override
    public void resume() {
        LOG.debug("resume");
    }

    @Override
    public void dispose() {
        LOG.debug("dispose");
        stage.dispose();
    }

    public Map<String, AssetLoaderParameters> getRequiredAssetsParameters() {
        return new TreeMap<>();
    }

    public Map<String, Class> getRequiredAssets() {
        return new TreeMap<>();
    }

    public List<String> getRequiredMusic() {
        return new ArrayList<>();
    }

    public List<String> getRequiredSfx() {
        return new ArrayList<>();
    }

    public static float invertHeight(float y) {
        return GAME_VIEWPORT_HEIGHT - y;
    }

    public String getName() {
        return "undefined";
    }

}
