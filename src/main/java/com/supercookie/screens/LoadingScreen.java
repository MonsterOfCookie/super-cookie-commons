package com.supercookie.screens;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.supercookie.CookieGame;
import com.supercookie.assets.UI;

import java.util.Map;
import java.util.TreeMap;

public abstract class LoadingScreen<T extends CookieGame> extends AbstractScreen<T> {

    private final AbstractScreen<T> nextScreen;

    private float progress = 0;

    private boolean stillLoading = true;

    public LoadingScreen(AbstractScreen<T> nextScreen) {
        super(nextScreen.getGame());
        this.nextScreen = nextScreen;
    }

    @Override
    public void show() {
        super.show();

        Map<String, Class> toLoad = new TreeMap<>();
        Map<String, Class> required = nextScreen.getRequiredAssets();
        for (Map.Entry<String, Class> entry : required.entrySet()) {
            if (UI.LOADER.needsToLoad(entry.getKey())) {
                toLoad.put(entry.getKey(), entry.getValue());
            }
        }

        for (String music : nextScreen.getRequiredMusic()) {
            toLoad.put(music, Music.class);
        }

        for (String sfx : nextScreen.getRequiredSfx()) {
            toLoad.put(sfx, Sound.class);
        }

        UI.LOADER.assetsToLoad(toLoad, nextScreen.getRequiredAssetsParameters());

        addCustomLoadingDetail();
    }

    protected abstract void addCustomLoadingDetail();

    protected abstract void updateCustomLoadingDetail(float delta, float progress);

    @Override
    protected void update(float delta) {
        super.update(delta);
        if (stillLoading) {
            progress = UI.LOADER.getProgress();
            updateCustomLoadingDetail(delta, progress);
            if (UI.LOADER.update()) {
                stillLoading = false;
                assetLoadingComplete();
            }
        }
    }

    public void assetLoadingComplete() {
        game.setScreen(nextScreen);
    }

    @Override
    public void pause() {
        LOG.debug("pause - loading screen override");
    }

    @Override
    public void resume() {
        LOG.debug("resume - loading screen override");
    }

}
