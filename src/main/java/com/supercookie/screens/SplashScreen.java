package com.supercookie.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;
import com.supercookie.CookieGame;
import com.supercookie.assets.UI;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;

public class SplashScreen<T extends CookieGame> extends AbstractScreen<T> {

    private static final String SPLASH_SCREEN_NAME = "splash.png";

    private final AbstractScreen<T> nextScreen;

    private Texture splash;

    public SplashScreen(T game, AbstractScreen<T> nextScreen) {
        super(game);
        this.nextScreen = nextScreen;
    }

    @Override
    public String getName() {
        return "SPLASH";
    }

    @Override
    public void show() {
        super.show();
        splash = new Texture(Gdx.files.internal(SPLASH_SCREEN_NAME));
        Image splashImage = new Image(splash);
        splashImage.setScaling(Scaling.fit);
        splashImage.setFillParent(true);
        stage.addActor(splashImage);
        stage.addAction(delay(1F, new Action() {
            @Override
            public boolean act(float delta) {
                game.setScreen(getGame().getLoadingScreen(nextScreen));
                return true;
            }
        }));
    }

    @Override
    public void dispose() {
        super.dispose();
        splash.dispose();
        UI.LOADER.disposeAll();
    }
}
