package com.supercookie;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.supercookie.screens.AbstractScreen;
import com.supercookie.screens.LoadingScreen;
import com.supercookie.screens.SplashScreen;

public abstract class CookieGame<T extends CookieGame<T>> extends Game {

    @Override
    public void create() {

        switch (Gdx.app.getType()) {
            case Android: {
                Gdx.app.setLogLevel(Application.LOG_ERROR);
            }
            break;
            case iOS: {
                Gdx.app.setLogLevel(Application.LOG_ERROR);
            }
            break;
            default: {
                Gdx.app.setLogLevel(Application.LOG_DEBUG);
            }
        }

        Gdx.input.setCatchBackKey(true);
        Gdx.input.setCatchMenuKey(true);

        setScreen(new SplashScreen<>((T) this, getPostSplashScreen()));
    }

    @Override
    public void setScreen(Screen screen) {
        if (getScreen() != null) getScreen().dispose();
        super.setScreen(screen);
    }

    public void setScreenWithLoading(AbstractScreen<T> screen) {
        setScreen(getLoadingScreen(screen));
    }

    @Override
    public void render() {
        super.render();
        MessageManager.getInstance().update(Gdx.graphics.getRawDeltaTime());
    }

    public abstract LoadingScreen<T> getLoadingScreen(AbstractScreen<T> abstractScreen);

    public abstract AbstractScreen<T> getPostSplashScreen();
}

